"""Common configuration constants
"""

PROJECTNAME = 'cop.semantic'

ADD_PERMISSIONS = {
    # -*- extra stuff goes here -*-
}

REPOSITORY = 'cop_semantic'
TRIPLESTORE_URL = 'http://localhost:8088/openrdf-sesame/repositories'
