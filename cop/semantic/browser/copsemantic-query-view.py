# -*- coding: utf-8 -*-
from zope.component.hooks import getSite
from Products.Five.browser import BrowserView
from communities.practice.generics.vocabularies import COMMUNITY_DOMAIN_VOCABULARY
from cop.semantic.generics.queries import QUERIES
from cop.semantic.config import REPOSITORY
from cop.semantic.config import TRIPLESTORE_URL


class CoPSemanticQueryView(BrowserView):
    """Classe com metodo para importar conteudo das comunidades gerando registros de colaboracao
    """

    def get_communities(self):
        """Retorna uma lista com todas comunidades do portal
        """
        portal = getSite()
        portal_path = "/".join(portal.getPhysicalPath())
        communities = self.context.portal_catalog(
            path=portal_path,
            portal_type="CoP",
            sort_on="sortable_title",
        )
        list_communities = []
        for community in communities:
            list_communities.append({
                "UID": community.UID,
                "title": community.Title,
                "url": community.getURL(),
            })

        return list_communities

    def get_domains(self):
        """
        """
        domains = []
        for domain in COMMUNITY_DOMAIN_VOCABULARY:
            domains.append(domain)
        return domains

    def get_queries(self):
        """
        """
        return [
            ("dominio_cop" , u"Communities that share the same domain"),
            ("comentarios" , u"Users that comment about the same domain"),
            ("participantes" , u"Users that participate in the same community"),
            ("portfolio" , u"Communities that had activity in the portfolios"),
        ]

    def do_query(self):
        """
        """
        import urllib
        import httplib2
        import json
        result = ""
        form = self.request.form
        query = form.get("query_select")
        if "submit_query" in form.keys() and query:
            full_query = QUERIES.get(query)
            params = {}
            if query in ["dominio_cop", "comentarios"]:
                domain = "<%s>" % form.get("domain_select")
                full_query = full_query % domain
            elif query == "participantes":
                community = "<%s>" % form.get("community_select")
                full_query  = full_query % community
            params["query"] = full_query
            headers = { 
              'content-type': 'application/x-www-form-urlencoded',
              'accept': 'application/sparql-results+json',
            }
            endpoint = "%s/%s" % (TRIPLESTORE_URL, REPOSITORY)
            (response, content) = httplib2.Http().request(endpoint, "POST", urllib.urlencode(params), headers=headers)
            if content:
                results = json.loads(content)
                #result = "<br>".join([" - ".join([result[key]['value'] for key in result.keys()]) for result in results['results']['bindings']])
                result_html = "<table class='listing' style='font-size: 9pt;'>"
                if results['results']['bindings']:
                    titles = results['results']['bindings'][0].keys()
                    for title in titles:
                        result_html += "<th>%s</th>" % title
                    for result in results['results']['bindings']:
                        result_html += "<tr>"
                        for key in result.keys():
                            if result[key]['type'] == 'uri':                            
                                result_html += "<td><a href='%s'>%s</a></td>" % (result[key]['value'], result[key]['value'])
                            else:
                                result_html += "<td>%s</td>" % result[key]['value']
                        result_html += "</tr>"
                result_html += "</table>"
                result = result_html    

        return result
