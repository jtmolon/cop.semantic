# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from cop.semantic.generics.generics import insertCollaborationRegister
from cop.semantic.generics.generics import insertCommunityOfPractice
from cop.semantic.generics.generics import getCoPMenuContext
from communities.practice.generics.generics import getCoPContext
from zope.component.hooks import getSite
from datetime import datetime

class ImportRDFView(BrowserView):
    """Classe com metodo para importar conteudo das comunidades gerando registros de colaboracao
    """

    def import_rdf(self):
        portal = getSite()
        catalog = portal.portal_catalog
        communities = catalog(portal_type="CoP")
        for community in communities:
            cop_object = community.getObject()
            community_uri = cop_object.absolute_url()
            name = community.Title
            creator_uri = community.Creator
            creation_date = datetime.now()
            domain = cop_object.getCommunity_domain()
            insertCommunityOfPractice(community_uri, name, creator_uri, creation_date, domain)

            #inserir local roles das comunidades no triple store
            local_roles = cop_object.get_local_roles()
            for local_role in local_roles:
                register_uri = "%s/configuracoes#%s" % (cop_object.absolute_url(), local_role[0])
                community_uri = cop_object.absolute_url()
                creator_uri = local_role[0]
                content_type = local_role[1][0]
                content_uri = None
                creation_date = datetime.now()
                register_type = "Participate"
                community_context = "configuracoes"
                domain = cop_object.Title()
                insertCollaborationRegister(register_uri, community_uri, creator_uri, content_type, content_uri,
                                            creation_date, register_type, community_context, domain)
                        



        all_content = catalog(portal_type=["CoPDocument", "CoPImage", "CoPFile", "CoPLink", "CoPEvent", "CoPATA",
                                           "CoPPortfolio", "CoPUpload", "PloneboardForum", "PloneboardConversation",
                                           "PloneboardComment", "Discussion Item"])    
        for content in all_content:
            content_object = content.getObject()
            community = getCoPContext(content_object)
            if community:
                if content.portal_type == "Discussion Item":
                    """se for discussion item tem que pegar a URL do pai, colocar um /view e colocar o id do comentario
                    """
                    prefix = content.getURL()[:content.getURL().find("++")]
                    suffix = content.getURL()[content.getURL().rfind("/")+1:]
                    content_uri = "%sview#%s" % (prefix, suffix)
                else:
                    content_uri = content.getURL()
                register_uri = content.getURL()
                community_uri = community.absolute_url()
                creator_uri = content.Creator
                content_type = content.portal_type
                creation_date = content.created
                register_type = "CreateContent"
                copmenu = getCoPMenuContext(content_object)
                community_context = None
                if copmenu:
                    community_context = copmenu.titlemenu
                if content_type not in ["Discussion Item", "PloneboardComment",]:
                    domain = content_object.Title()
                elif content_type == "Discussion Item":
                    domain = content_object.text
                else:
                    domain = content_object.getText()
                insertCollaborationRegister(register_uri, community_uri, creator_uri, content_type, content_uri,
                                            creation_date, register_type, community_context, domain)
