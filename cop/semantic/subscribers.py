# -*- coding: utf-8 -*-
from communities.practice.generics.generics import getCoPContext
from datetime import datetime
from cop.semantic.generics.generics import insertCollaborationRegister
from cop.semantic.generics.generics import insertCommunityOfPractice
from cop.semantic.generics.generics import deleteCollaborationRegister
from cop.semantic.generics.generics import deleteMemberRoles
from cop.semantic.generics.generics import getCoPMenuContext
from zope.component.hooks import getSite

def insertContentCollaborationRegister(obj, event):
    """Insere o Registro de Colaboracao na criacao de conteudos
    """
    portal = getSite()
    qi = getattr(portal, 'portal_quickinstaller')
    if qi.isProductInstalled('cop.semantic'):
        cop = getCoPContext(obj)
        #cria o registro somente se o conteudo for criado no contexto de uma CoP
        if cop:
            register_uri = obj.absolute_url()
            community_uri = cop.absolute_url()
            creator_uri = obj.Creator()
            content_type = obj.portal_type
            if content_type == "Discussion Item":
                """se for discussion item tem que pegar a URL do pai, colocar um /view e colocar o id do comentario
                """
                prefix = obj.absolute_url()[:obj.absolute_url().find("++")]
                suffix = obj.absolute_url()[obj.absolute_url().rfind("/")+1:]
                content_uri = "%sview#%s" % (prefix, suffix)
            else:
                content_uri = obj.absolute_url()
            creation_date = datetime.now()
            register_type = "CreateContent"
            copmenu = getCoPMenuContext(obj)
            community_context = copmenu.titlemenu
            if content_type not in ["Discussion Item", "PloneboardComment",]:
                domain = obj.Title()
            elif content_type == "Discussion Item":
                domain = obj.text
            else:
                domain = obj.getText()
            insertCollaborationRegister(
                register_uri, community_uri, creator_uri,
                content_type, content_uri, creation_date,
                register_type, community_context, domain,
            )

    return True

def insertTripleCommunityOfPractice(obj, event):
    """
    """
    portal = getSite()
    qi = getattr(portal, 'portal_quickinstaller')
    if qi.isProductInstalled('cop.semantic'):
        community_uri = obj.absolute_url()
        name = obj.Title()
        creator_uri = obj.Creator()
        creation_date = datetime.now()
        domain = obj.getCommunity_domain()
        insertCommunityOfPractice(community_uri, name, creator_uri, creation_date, domain)
    return True

def editContentCollaborationRegister(obj, event):
    """Remove o registro antigo e cria um novo
    """
    portal = getSite()
    qi = getattr(portal, 'portal_quickinstaller')
    if qi.isProductInstalled('cop.semantic'):
        cop = getCoPContext(obj)
        if cop:
            register_uri = obj.absolute_url()
            deleteCollaborationRegister(register_uri)
            community_uri = cop.absolute_url()
            creator_uri = obj.Creator()
            content_type = obj.portal_type
            content_uri = obj.absolute_url()
            creation_date = datetime.now()
            register_type = "CreateContent"
            copmenu = getCoPMenuContext(obj)
            community_context = copmenu.titlemenu
            insertCollaborationRegister(
                register_uri, community_uri, creator_uri,
                content_type, content_uri, creation_date,
                register_type, community_context, None,
            )

    return True


def copLocalRolesChanged(obj, event):
    """ Adds register to triple store when
        local roles in CoP are changed
    """
    portal = getSite()
    qi = getattr(portal, 'portal_quickinstaller')
    if qi.isProductInstalled('cop.semantic'):
        cop = obj
        if not getattr(cop, "portal_type", "") == "CoP":
            cop = getCoPContext(cop)
        if cop:        
            register_uri = "%s/configuracoes#%s" % (cop.absolute_url(), event.member_id)
            deleteMemberRoles(register_uri)
            community_uri = cop.absolute_url()
            creator_uri = event.member_id
            content_type = event.role
            content_uri = None
            creation_date = datetime.now()
            register_type = "Participate"
            community_context = "configuracoes"
            domain = cop.Title()
            insertCollaborationRegister(
                register_uri, community_uri, creator_uri, 
                content_type, content_uri, creation_date,
                register_type, community_context, domain,
            )
