# -*- coding: utf-8 -*-
from zope.component.hooks import getSite
from rdflib import ConjunctiveGraph
from rdflib import Namespace
from rdflib.namespace import FOAF
from rdflib.namespace import XSD
from rdflib import URIRef
from rdflib import Literal
from cop.semantic.config import REPOSITORY
from cop.semantic.config import TRIPLESTORE_URL
from Acquisition import aq_inner
from Acquisition import aq_chain
from rdfalchemy import rdfSubject
from rdfalchemy import rdfSingle
import urllib
import httplib2

COPSEMANTIC = Namespace("http://localhost:8080/copsemantic/rdfs-copsemantic#")
COPONTOLOGY = Namespace("http://localhost:8080/copsemantic/owl-copontology#")
SIOC = Namespace("http://rdfs.org/sioc/ns#")

def insertCollaborationRegister(register_uri, community_uri, creator_uri, portal_type, content_uri,
                                creation_date, register_type, community_context, domain):
    """Insere o Registro de Colaboracao
    """
    portal = getSite()    
    graph = ConjunctiveGraph()
    graph.bind("foaf", FOAF)
    graph.bind("xsd", XSD)
    graph.bind("copsemantic", COPSEMANTIC)
    graph.bind("copontology", COPONTOLOGY)

    CollaborationRegister.db = graph
    collaboration_register = CollaborationRegister(URIRef(register_uri))
    collaboration_register.community_id = URIRef(community_uri)
    collaboration_register.creator_id = URIRef("%s/author/%s" % (portal.absolute_url(), creator_uri)) 
    if portal_type:
        collaboration_register.content_type = Literal(portal_type)
    if content_uri:
        collaboration_register.content_id = URIRef(content_uri)
    collaboration_register.creation_date = Literal(creation_date.strftime("%Y-%m-%d"))
    collaboration_register.register_type = Literal(register_type)
    if community_context:
        collaboration_register.cop_context = Literal(community_context)
    if domain:
        collaboration_register.domain = Literal(domain)

    context = portal.absolute_url()
    params = {}
    params["context"] = "<%s>" % context
    endpoint = "%s/%s/statements?%s" % (TRIPLESTORE_URL, REPOSITORY, urllib.urlencode(params))
    data = graph.serialize(format="xml")
    dict_headers = {}
    dict_headers["content-type"] = "application/rdf+xml"
    (response, content) = httplib2.Http().request(endpoint, "POST", body=data, headers=dict_headers)

    return True

def insertCommunityOfPractice(community_uri, name, creator_uri, creation_date, domain):
    """Insere uma CommunityOfPractice
    """
    portal = getSite()    
    graph = ConjunctiveGraph()
    graph.bind("sioc", SIOC)
    graph.bind("xsd", XSD)
    graph.bind("copsemantic", COPSEMANTIC)
    graph.bind("copontology", COPONTOLOGY)

    CommunityOfPractice.db = graph
    community_practice = CommunityOfPractice(URIRef(community_uri))
    community_practice.community_id = URIRef(community_uri)
    community_practice.name = Literal(name)
    community_practice.creator_id = URIRef("%s/author/%s" % (portal.absolute_url(), creator_uri)) 
    community_practice.creation_date = Literal(creation_date.strftime("%Y-%m-%d"))
    community_practice.domain = URIRef(domain)

    context = portal.absolute_url()
    params = {}
    params["context"] = "<%s>" % context
    endpoint = "%s/%s/statements?%s" % (TRIPLESTORE_URL, REPOSITORY, urllib.urlencode(params))
    data = graph.serialize(format="xml")
    dict_headers = {}
    dict_headers["content-type"] = "application/rdf+xml"
    (response, content) = httplib2.Http().request(endpoint, "POST", body=data, headers=dict_headers)

    return True

def deleteCollaborationRegister(register_uri):
    """
    """
    params = {}
    query = "DELETE {<%s> ?prop ?object} WHERE {<%s> ?prop ?object . ?reg ?prop ?object}" % (register_uri, register_uri)
    params["update"] = query
    endpoint = "%s/%s/statements?%s" % (TRIPLESTORE_URL, REPOSITORY, urllib.urlencode(params))
    (response, content) = httplib2.Http().request(endpoint, "POST")

    return True

def deleteMemberRoles(register_uri):
    """
    """
    params = {}
    query = "PREFIX copsemantic:<http://localhost:8080/copsemantic/rdfs-copsemantic#>"
    query += "DELETE {<%s> copsemantic:content_type ?object} " % register_uri
    query += "WHERE {<%s> copsemantic:content_type ?object . ?reg ?prop ?object}" % register_uri
    params["update"] = query
    endpoint = "%s/%s/statements?%s" % (TRIPLESTORE_URL, REPOSITORY, urllib.urlencode(params))
    (response, content) = httplib2.Http().request(endpoint, "POST")

    return True

def getCoPMenuContext(obj):
    """ Bottom-up function that returns first CoPMenu.
    """
    for parent in aq_chain(aq_inner(obj)):
        if hasattr(parent, "portal_type"):
            if parent.portal_type == "CoPMenu": 
                return parent
    return False

class CollaborationRegister(rdfSubject):
    """Classe do registro de colaboracao
        copsemantic:id_comunidade
        copsemantic:id_criador
        copsemantic:tipo_conteudo
        copsemantic:id_conteudo
        copsemantic:data_criacao
    """
    rdf_type = COPSEMANTIC.CollaborationRegister
    community_id = rdfSingle(COPSEMANTIC.community_id, 'community_id')
    creator_id = rdfSingle(COPSEMANTIC.creator_id, 'creator_id')
    content_type = rdfSingle(COPSEMANTIC.content_type, 'content_type')
    content_id = rdfSingle(COPSEMANTIC.content_id, 'content_id')
    creation_date = rdfSingle(COPSEMANTIC.creation_date, 'creation_date')
    register_type = rdfSingle(COPSEMANTIC.register_type, 'register_type')
    cop_context = rdfSingle(COPSEMANTIC.cop_context, 'cop_context')
    domain = rdfSingle(COPSEMANTIC.domain, 'domain')

class CommunityOfPractice(rdfSubject):
    """Classe da comunidade de pratica
    """
    rdf_type = COPSEMANTIC.CommunityOfPractice
    community_id = rdfSingle(COPSEMANTIC.community_id, 'community_id')
    name = rdfSingle(SIOC.title, 'title')
    creator_id = rdfSingle(COPSEMANTIC.creator_id, 'creator_id')
    creation_date = rdfSingle(COPSEMANTIC.creation_date, 'creation_date')
    domain = rdfSingle(COPSEMANTIC.domain, 'domain')
