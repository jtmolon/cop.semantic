# -*- coding: utf-8 -*-
QUERIES = {}
DOMINIO_COP = "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
DOMINIO_COP += " PREFIX copsemantic:<http://localhost:8080/copsemantic/rdfs-copsemantic#>"
DOMINIO_COP += " SELECT DISTINCT ?community"
DOMINIO_COP += " WHERE {"
DOMINIO_COP += " ?community rdf:type copsemantic:CommunityOfPractice ."
DOMINIO_COP += " ?community copsemantic:domain %s ."
DOMINIO_COP += " }" 

COMENTARIOS = "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
COMENTARIOS += " PREFIX copsemantic:<http://localhost:8080/copsemantic/rdfs-copsemantic#>"
COMENTARIOS += " SELECT DISTINCT ?user ?community"
COMENTARIOS += " WHERE {"
COMENTARIOS += " ?community rdf:type copsemantic:CommunityOfPractice ."
COMENTARIOS += " ?community copsemantic:domain %s ."
COMENTARIOS += " ?comment copsemantic:community_id ?community ."
COMENTARIOS += " ?comment copsemantic:creator_id ?user ."
COMENTARIOS += " ?comment copsemantic:content_type ?content ."
COMENTARIOS += " FILTER(?content = \"Discussion Item\" || ?content = \"PloneboardComment\") ."
COMENTARIOS += " }"
COMENTARIOS += " ORDER BY ?user"


PARTICIPANTES = "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
PARTICIPANTES += " PREFIX copsemantic:<http://localhost:8080/copsemantic/rdfs-copsemantic#>"
PARTICIPANTES += " SELECT DISTINCT ?member"
PARTICIPANTES += " WHERE {"
PARTICIPANTES += " ?participant copsemantic:community_id %s ."
PARTICIPANTES += " ?participant copsemantic:register_type \"Participate\" ."
PARTICIPANTES += " ?participant copsemantic:creator_id ?member ."
PARTICIPANTES += " }"

PORTFOLIO = "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
PORTFOLIO += " PREFIX copsemantic:<http://localhost:8080/copsemantic/rdfs-copsemantic#>"
PORTFOLIO += " SELECT DISTINCT ?community"
PORTFOLIO += " WHERE {"
PORTFOLIO += " ?register copsemantic:cop_context \"portfolio\" ."
PORTFOLIO += " ?register copsemantic:content_type ?content ."
PORTFOLIO += " FILTER(?content != \"CoPPortfolio\") ."
PORTFOLIO += " ?register copsemantic:community_id ?community ."
PORTFOLIO += " }"

QUERIES["dominio_cop"] = DOMINIO_COP
QUERIES["comentarios"] = COMENTARIOS
QUERIES["participantes"] = PARTICIPANTES
QUERIES["portfolio"] = PORTFOLIO
